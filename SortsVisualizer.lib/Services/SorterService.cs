﻿using System.Collections.ObjectModel;
using SortsVisualizer.lib.Interfaces;
using SortsVisualizer.lib.Models;
using SortsVisualizer.lib.Models.Base;
using SortsVisualizer.lib.Models.Sorts;

namespace SortsVisualizer.lib.Services;

public class SorterService : ISorterService
{
    private readonly Dictionary<SortType, BaseSorting> _sorters;
    private BaseSorting _startedStrategy = null!;

    public SorterService(Bubble bubble, Shaker shaker, Insertion insertion, Quick quick, Merge merge)
    {
        _sorters = new()
        {
            { SortType.Bubble, bubble},
            { SortType.Shaker, shaker },
            { SortType.Insertion, insertion },
            { SortType.Quick, quick},
            { SortType.Merge, merge},
        };
    }

    public async Task StartAsync(SortType type, ObservableCollection<DiagramItem> collection, int delay = 80)
    {
        if (!_sorters.ContainsKey(type))
            throw new ArgumentException(nameof(type));
        _startedStrategy = _sorters[type];
        await _startedStrategy.StartAsync(collection, delay);
    }

    public void Stop()
    {
        _startedStrategy?.Stop();
        _startedStrategy = null!;
    }

    public string[] GetSortersTypes()
    {
        return _sorters.Select(s => s.Key.ToString()).ToArray();
    }
}