﻿using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Media;
using SortsVisualizer.lib.Data;
using SortsVisualizer.lib.Interfaces;
using SortsVisualizer.lib.Models;
using Color = System.Windows.Media.Color;
namespace SortsVisualizer.lib.Services;

public class DiagramSourceService : IDiagramSourceService
{

    private static readonly Random Rnd = new Random();

    public Color BaseColor => Colors.White;
    public Color MovementColor => Colors.Orange;
    public Color SortedColor => Colors.Green;

    public ObservableCollection<DiagramItem> Collection { get; private set; }

    public DiagramSourceService(
        int elementsCount = 20,
        int heightFactor = 25,
        int width = 50
        )
    {
        Collection = CollectionCreator.GetCollection(new SolidColorBrush(Colors.White), elementsCount, heightFactor, width);
        Shuffle(ShuffleTypes.Random);
    }

    public void Shuffle(ShuffleTypes type)
    {
        switch(type)
        {
            case ShuffleTypes.Random:
            {
                for (int i = 0; i < 2; i++)
                for (int j = 0; j < 20 - 3; j++)
                {
                    var r = Rnd.Next(1, 3);
                    (Collection[j], Collection[j + r]) = (Collection[j + r], Collection[j]);
                }

                for (int i = 0; i < 2; i++)
                for (int j = 19; j > 3; j--)
                {
                    var r = Rnd.Next(1, 3);
                    (Collection[j], Collection[j - r]) = (Collection[j - r], Collection[j]);
                }
                break;
            }
            case ShuffleTypes.Reverse:
            {
                DiagramItem temp;
                for (int i = 0; i < Collection.Count; i++)
                {
                    for (int j = i + 1; j < Collection.Count; j++)
                    {
                        if (Collection[i].Value < Collection[j].Value)
                        {
                            temp = Collection[i];
                            Collection[i] = Collection[j];
                            Collection[j] = temp;
                        }
                    }
                }
                break;
            }
            
        }


    }


    /// <summary>
    /// Замена цвета заливки элемента коллекции
    /// </summary>
    /// <param name="index">Индекс элемента</param>
    /// <param name="color">Новый цвет</param>
    [Obsolete]
    public void ChangeColor(
        int index,
        System.Windows.Media.Color color)
    {
        var newItem = Collection[index];
        newItem.Color = new SolidColorBrush(color);
        Collection[index] = newItem;
    }


    /// <summary>
    /// Замена цвета заливки элемента коллекции
    /// </summary>
    /// <param name="index">Индекс элемента</param>
    /// <param name="color">Новый цвет</param>
    public void ChangeColorButGreen(
        int index,
        Color color)
    {
        var newItem = Collection[index];
        if (newItem.Color.Color == SortedColor) return;
        newItem.Color = new SolidColorBrush(color);
        Collection[index] = newItem;
    }


    /// <summary>
    /// Анимация заливки выбранным цветом всех не закрашенных этим цветом элементов.
    /// </summary>
    /// <param name="cancel"></param>
    /// <param name="color"></param>
    /// <param name="delay">Задержка отрисовки цвета</param>
    /// <returns></returns>
    public async Task MakeLadderAnimation(
        CancellationToken cancel,
        Color color,
        ObservableCollection<DiagramItem> items,
        int delay = 50)
    {
        for (int i = 0; i < Collection.Count; i++)
        {
            if (Collection[i].Color.Color == color)
                continue;
            ChangeColor(i, color);
            await Task.Delay(delay, cancel);
        }
    }
}