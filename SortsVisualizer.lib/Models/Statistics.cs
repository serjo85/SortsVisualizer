﻿namespace SortsVisualizer.lib.Models;

public class Statistics
{
    private readonly StatisticObserver _statisticObserver;
    private int _replacement;
    private int _comparison;

    public Statistics(StatisticObserver statisticObserver)
    {
        _statisticObserver = statisticObserver;
    }

    public int Replacement
    {
        get => _replacement;
        set
        {
            _replacement = value;
            _statisticObserver.Update(this);
        }
    }

    public int Comparison
    {
        get => _comparison;
        set
        {
            _comparison = value;
            _statisticObserver.Update(this);
        }
    }

    public void Reset()
    {
        Comparison = 0;
        Replacement = 0;
        _statisticObserver.Update(this);
    }
}