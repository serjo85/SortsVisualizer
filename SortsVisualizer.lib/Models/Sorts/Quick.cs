﻿using SortsVisualizer.lib.Interfaces;
using SortsVisualizer.lib.Models.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;

namespace SortsVisualizer.lib.Models.Sorts
{
    public class Quick: BaseSorting
    {
        public Quick(IDiagramSourceService diagramService, Statistics statistics) : base(diagramService, statistics)
        {
        }

        public override async Task SortAsync(ObservableCollection<DiagramItem> collection, CancellationToken cancel, int delay)
        {
            var array = collection.Select(x => x.Value).ToArray();
            await QuickSortAsync(array, 0, array.Length - 1, 500, cancel);
        }

        private async Task QuickSortAsync(int[] array, int low, int high, int delay, CancellationToken cancel)
        {
            if (low < high)
            {
                int pivot = await PartitionAsync(array, low, high, delay, cancel);

                if (pivot > 1)
                {
                    await QuickSortAsync(array, low, pivot - 1, delay, cancel);
                }

                if (pivot + 1 < high)
                {
                    await QuickSortAsync(array, pivot + 1, high, delay, cancel);
                }
            }
        }


        private async Task<int> PartitionAsync(int[] array, int low, int high, int delay, CancellationToken cancel)
        {
            int pivot = array[low];

            while (true)
            {
                while (array[low] < pivot)
                {
                    low++;
                    Statistics.Comparison++;
                }

                while (array[high] > pivot)
                {
                    high--;
                    Statistics.Comparison++;
                }

                if (low < high)
                {
                    if (array[low] == array[high])
                    {
                        Statistics.Comparison++;
                        SortedPaint(low);
                        return high;
                    }
                        

                    (array[low], array[high]) = (array[high], array[low]);
                    MovementPaint(low);
                    await Task.Delay(delay,cancel);
                    Swap(low, high);
                    Statistics.Replacement++;
                    await Task.Delay(delay, cancel);
                }
                else
                {
                    SortedPaint(low);
                    SortedPaint(high);
                    return high;
                }
            }
        }
    }
}
