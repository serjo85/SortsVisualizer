﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using SortsVisualizer.lib.Interfaces;
using SortsVisualizer.lib.Models.Base;

namespace SortsVisualizer.lib.Models.Sorts
{
    public class Insertion : BaseSorting
    {
        public Insertion(IDiagramSourceService diagramService, Statistics statistics) : base(diagramService, statistics)
        {
        }

        public override async Task SortAsync(
        ObservableCollection<DiagramItem> collection,
        CancellationToken cancel,
        int delay)
        {
            int j;
            int x;
            SortedPaint( 0);
            for (int i = 1; i < collection.Count; i++)
            {
                x = collection[i].Value;
                j = i;
                while (j > 0 && collection[j - 1].Value > x)
                {
                    MovementPaint(j);
                    await Task.Delay(delay, cancel);
                    Swap(j, j - 1);
                    await Task.Delay(delay, cancel);
                    Statistics.Replacement++;
                    Statistics.Comparison++;
                    j -= 1;
                }
                SortedPaint(j);
            }
        }
    }
}
