﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using SortsVisualizer.lib.Data;
using SortsVisualizer.lib.Interfaces;
using SortsVisualizer.lib.Models.Base;

namespace SortsVisualizer.lib.Models.Sorts
{
    public class Merge : BaseSorting
    {
        public Merge(IDiagramSourceService diagramService, Statistics statistics) : base(diagramService, statistics)
        {}

        public override async Task SortAsync(ObservableCollection<DiagramItem> collection, CancellationToken cancel, int delay)
        {
            var array = collection.Select(x => x.Value).ToArray();
            int l;
            int r;
            int m;
            l = 0;
            r = array.Length - 1;
            m = l + (r - l) / 2;
            await MergeSort(CollectionCreator.GetCollection(new SolidColorBrush(Colors.White)).ToArray(), l, m, r, collection, cancel, delay);
        }

        private async Task MergeSort(DiagramItem[] array, int l, int m, int r, ObservableCollection<DiagramItem> collection, CancellationToken cancel, int delay)
        {
            int i, j, k;

            int n1 = m - l + 1;
            int n2 = r - m;

            DiagramItem[] left = new DiagramItem[n1 + 1];
            DiagramItem[] right = new DiagramItem[n2 + 1];

            for (i = 0; i < n1; i++)
            {
                left[i] = array[l + i];
                Statistics.Replacement++;
            }

            for (j = 1; j <= n2; j++)
            {
                right[j - 1] = array[m + j];
                Statistics.Replacement++;
            }

            left[n1].Value = int.MaxValue;
            right[n2].Value = int.MaxValue;

            i = 0;
            j = 0;

            for (k = l; k <= r; k++)
            {
                if (left[i].Value < right[j].Value)
                {
                    array[k] = left[i];
                    collection[k] = left[i];
                    Statistics.Replacement++;
                    await Task.Delay(delay, cancel);
                    i = i + 1;
                }
                else
                {
                    array[k] = right[j];
                    collection[k] = right[j];
                    Statistics.Replacement++;
                    await Task.Delay(delay, cancel);
                    j = j + 1;

                }
            }
        }
    }
}

