﻿using System.Collections.ObjectModel;
using System.Windows.Media;
using SortsVisualizer.lib.Interfaces;
using SortsVisualizer.lib.Models.Base;

namespace SortsVisualizer.lib.Models.Sorts;

public class Bubble : BaseSorting
{
    public Bubble(IDiagramSourceService diagramService, Statistics statistics) : base(diagramService, statistics)
    {
    }

    public override async Task SortAsync(
        ObservableCollection<DiagramItem> collection,
        CancellationToken cancel, int delay)
    {
        int num = collection.Count;
        for (int i = 0; i < num - 1; i++)
        {
            var hasSwap = false;
            
            for (int j = 0; j < num - i - 1; j++)
            {
                // j - индекс текущего элемента.
                Statistics.Comparison++;
                // Закрашиваем текущий элемент с перестановкой.
                if (collection[j].Value > collection[j + 1].Value)
                {
                    hasSwap = true;

                    if (j > 0)
                        BasePaint( j - 1);
                    MovementPaint(j);
                    await Task.Delay(delay, cancel);
                    Swap(j, j + 1);
                    Statistics.Replacement++;
                }
                // Закрашиваем текущий элемент без перестановки.
                else
                {
                    if (j > 0)
                        BasePaint(j - 1);
                    MovementPaint(j);
                    await Task.Delay(delay, cancel);
                }
            }

            // Отмечаем зелёным последний элемент как отсортированный.
            SortedPaint( num - i - 1);
            /*  Красим в белый предпоследний прямоугольник иначе
                на последней итерации будет оранжевая полоса.
                TODO Нужно оптимизировать.
            */
            BasePaint(num - i - 2);

            Statistics.Comparison++;
            // Проход без замены признак отсортированной последовательности.
            if (hasSwap == false)
            {
                return;
            }
        }
    }
}