﻿using System.Collections.ObjectModel;
using System.Windows.Media;
using System.Drawing;
using SortsVisualizer.lib.Models.Base;
using System;
using SortsVisualizer.lib.Interfaces;

namespace SortsVisualizer.lib.Models.Sorts;

public class Shaker : BaseSorting
{
    public Shaker(IDiagramSourceService diagramService, Statistics statistics) : base(diagramService, statistics)
    {
    }

    public override async Task SortAsync(
        ObservableCollection<DiagramItem> collection,
        CancellationToken cancel, int delay)
    {
        bool swapped;
        int start = 0;
        int end = collection.Count - 1;

        do
        {
            // Проход слева направо
            swapped = false;

            for (int i = start; i < end; i++)
            {
                if(i > start) BasePaint(i - 1);
                MovementPaint(i);
                await Task.Delay(delay, cancel);
                if (collection[i].Value > collection[i + 1].Value)
                {
                    Swap(i, i + 1);
                    swapped = true;
                    Statistics.Replacement++;
                }

                Statistics.Comparison++;
            }
            SortedPaint(end);
            if (!swapped)
                break;

            end--;

            // Проход справа налево
            swapped = false;

            for (int i = end - 1; i >= start; i--)
            {
                if (i <= end - 1) BasePaint(i + 1);
                if (collection[i].Value > collection[i + 1].Value)
                {
                    Swap(i, i + 1);
                    swapped = true;
                    Statistics.Replacement++;
                }
                MovementPaint(i);
                Statistics.Comparison++;
                await Task.Delay(delay, cancel);
            }
            SortedPaint(start);
            start++;
        }
        while (swapped);

    }
}