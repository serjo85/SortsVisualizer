﻿using System.Data;

namespace SortsVisualizer.lib.Models
{
    public class StatisticObserver
    {
        public delegate void StatisticHandler(Statistics  statistics);
        public event StatisticHandler Handler;

        public void Update(Statistics statistics)
        {
            Handler?.Invoke(statistics);
        }
    }
}
