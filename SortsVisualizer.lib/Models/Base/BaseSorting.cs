﻿using System.Collections.ObjectModel;
using Microsoft.VisualBasic;
using SortsVisualizer.lib.Interfaces;
using static SortsVisualizer.lib.Models.Base.BaseSorting;

namespace SortsVisualizer.lib.Models.Base;

public abstract class BaseSorting
{
    protected CancellationTokenSource Cts = null!;
    protected IDiagramSourceService DiagramService;
    protected readonly Statistics Statistics;

    protected BaseSorting(IDiagramSourceService diagramService, Statistics statistics)
    {
        DiagramService = diagramService;
        Statistics = statistics;
    }

    public async Task StartAsync(ObservableCollection<DiagramItem> collection,int delay)
    {
        Cts = new CancellationTokenSource();
        try
        {
            Statistics.Reset();
            await DiagramService.MakeLadderAnimation(CancellationToken.None, System.Windows.Media.Colors.White, collection);
            await SortAsync(collection, Cts.Token,delay);
            await DiagramService.MakeLadderAnimation(CancellationToken.None, System.Windows.Media.Colors.Green, collection);
        }
        catch (OperationCanceledException e)
        {
            Console.WriteLine("Action was interrupted by user.");
        }
        finally
        {
            Cts.Dispose();
            Cts = new CancellationTokenSource();
            await DiagramService.MakeLadderAnimation(CancellationToken.None, System.Windows.Media.Colors.White, collection);
        }
    }

    public abstract Task SortAsync(ObservableCollection<DiagramItem> collection, CancellationToken cancel, int delay);

    protected void Swap(int index1, int index2)
    {
        DiagramItem temp = DiagramService.Collection[index1];
        DiagramService.Collection[index1] = DiagramService.Collection[index2];
        DiagramService.Collection[index2] = temp;
    }

    protected void BasePaint(int index)
    {
        DiagramService.ChangeColorButGreen(index, DiagramService.BaseColor);
    }

    protected void MovementPaint(int index)
    {
        DiagramService.ChangeColorButGreen(index, DiagramService.MovementColor);
    }

    protected void SortedPaint(int index)
    {
        DiagramService.ChangeColorButGreen(index, DiagramService.SortedColor);
    }

    public void Stop()
    {
        Cts.Cancel();
        Statistics.Reset();
    }
}