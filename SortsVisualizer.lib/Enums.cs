﻿namespace SortsVisualizer.lib;

public enum SortType
{
    Bubble,
    Shaker,
    Insertion,
    Quick,
    Merge,
}

public enum ShuffleTypes
{
    Random = 1,
    Reverse = 2,
}

public enum ProcessStates
{
    NothingToDo,
    Sorting,
    Shuffling,
}