﻿using System.Collections.ObjectModel;
using System.Windows.Media;
using SortsVisualizer.lib.Models;

namespace SortsVisualizer.lib.Data;

public static class CollectionCreator
{
    public static ObservableCollection<DiagramItem> GetCollection(SolidColorBrush baseColor, int elementsCount = 20, int heightFactor = 25, int width = 50)
    {
        var items = new ObservableCollection<DiagramItem>();
        for (int i = 1; i < elementsCount + 1; i++)
            items.Add(new DiagramItem
            {
                Value = i,
                Height = i * heightFactor,
                Width = width,
                Color = baseColor,
            });
        return items;
    }

    public static ObservableCollection<DiagramItem> GetReverseCollection(SolidColorBrush baseColor, int elementsCount = 20, int heightFactor = 25, int width = 50)
    {
        var items = new ObservableCollection<DiagramItem>();
        for (int i = 20; i > 0; i--)
            items.Add(new DiagramItem
            {
                Value = i,
                Height = i * heightFactor,
                Width = width,
                Color = baseColor,
            });
        return items;
    }
}