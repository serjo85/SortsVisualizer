﻿using System.Collections.ObjectModel;
using SortsVisualizer.lib.Models;

namespace SortsVisualizer.lib.Interfaces;

public interface ISorterService
{
    /// <summary>
    /// Старт сортировки
    /// </summary>
    /// <param name="type">Тип сортировки</param>
    /// <param name="collection">Сортируемая коллекция</param>
    /// <param name="delay">Задержка по умолчанию</param>
    /// <returns></returns>
    public Task StartAsync(SortType type, ObservableCollection<DiagramItem> collection, int delay);

    /// <summary>
    /// Остановить сортировку
    /// </summary>
    public void Stop();

    /// <summary>
    /// Получить список доступных сортировок
    /// </summary>
    /// <returns>Список сортировок</returns>
    public string[] GetSortersTypes();
}