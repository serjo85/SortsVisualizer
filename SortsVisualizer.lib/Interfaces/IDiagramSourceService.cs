﻿using System.Collections.ObjectModel;
using System.Windows.Media;
using SortsVisualizer.lib.Models;
using Color = System.Windows.Media.Color;
namespace SortsVisualizer.lib.Interfaces;

/// <summary>
/// Сервис работы с источником диаграммы сортировки
/// </summary>
public interface IDiagramSourceService
{
    public ObservableCollection<DiagramItem> Collection { get; }

    /// <summary>
    /// Цвет элементов по умолчанию
    /// </summary>
    public Color BaseColor { get; }

    /// <summary>
    /// Цвет перемещаемых элементов
    /// </summary>
    public Color MovementColor { get; }

    /// <summary>
    /// Цвет отсортированных элементов
    /// </summary>
    public Color SortedColor { get; }

    public void Shuffle(ShuffleTypes type);

    /// <summary>
    /// Замена цвета заливки элемента коллекции
    /// </summary>
    /// <param name="index">Индекс элемента</param>
    /// <param name="color">Новый цвет</param>
    public void ChangeColor(int index, Color color);

    /// <summary>
    /// Замена цвета заливки элемента коллекции
    /// </summary>
    /// <param name="index">Индекс элемента</param>
    /// <param name="color">Новый цвет</param>
    public void ChangeColorButGreen(int index, Color color);

    /// <summary>
    /// Анимация заливки выбранным цветом всех не закрашенных этим цветом элементов.
    /// </summary>
    /// <param name="cancel"></param>
    /// <param name="color"></param>
    /// <param name="delay">Задержка отрисовки цвета</param>
    /// <returns></returns>
    public Task MakeLadderAnimation(
        CancellationToken cancel,
        Color color,
        ObservableCollection<DiagramItem> items,
        int delay = 50);


}