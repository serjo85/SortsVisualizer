﻿using System;
using System.Windows;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using SortsVisualizer.lib;
using SortsVisualizer.lib.Interfaces;
using SortsVisualizer.lib.Models;
using SortsVisualizer.lib.Models.Sorts;
using SortsVisualizer.lib.Services;
using SortsVisualizer.UI.ViewModels;
using SortsVisualizer.UI.Views;

namespace SortsVisualizer.UI;

/// <summary>
/// Interaction logic for App.xaml
/// </summary>
public partial class App : Application
{
    private IServiceProvider _serviceProvider;

    protected override void OnStartup(StartupEventArgs e)
    {
        ConfigureServices();
        var vm = _serviceProvider.GetService<MainWindowViewModel>();
        var mainWindow = new MainWindow(vm);
        mainWindow.Show();
    }

    private void ConfigureServices()
    {
        var services = new ServiceCollection();
        services.AddSingleton<IDiagramSourceService, DiagramSourceService>();
        services.AddSingleton<ISorterService, SorterService>();
        services.AddSingleton<StatisticObserver>();
        services.AddSingleton<Statistics>();
        services.AddSingleton<MainWindowViewModel>();
        services.AddSingleton<Bubble>();
        services.AddSingleton<Shaker>();
        services.AddSingleton<Insertion>();
        services.AddSingleton<Quick>();
        services.AddSingleton<Merge>();
        _serviceProvider = services.BuildServiceProvider();
    }
}